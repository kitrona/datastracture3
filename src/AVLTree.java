import java.io.PrintWriter;

/* Class AVLTree */
class AVLTree
{
    private AVLNode root;

    /* Constructor */
    public AVLTree()
    {
       root = null;
    }
    /* Function to check if tree is empty */
    public boolean isEmpty()
    {
        return root == null;
    }
    /* Make the tree logically empty */
    public void makeEmpty()
    {
        this.root = null;
    }
    /* Function to insert data */
    public void insert(Comparable data)
    {
    	  root = insert(data, root);
    }
    /* Function to get height of node */
    private int height(AVLNode t )
    {
        int height =0;
        if( t==null)
        	return height;
        else
        	height ++;
        	return max(height(t.right),height(t.left));
    }
    /* Function to max of left/right node */
    private int max(int lhs, int rhs)
    {
    	if(lhs>rhs)
    		return lhs;
    	else
    		return rhs;
    }
    /* Function to insert data recursively */
    private AVLNode insert(Comparable x, AVLNode t)
    {
    	if(t==null){
    		t= new AVLNode(x);
    	}
    	else if(x.compareTo(t.data)>0){
    		t.right = insert(x,t.right);
    		if( height( t.right ) - height( t.left ) == 2 )
                if( x.compareTo( t.right.data) > 0 )
                    t = rotateWithRightChild( t );
                else
                    t = doubleWithRightChild( t );
    	}
    		
        else if(x.compareTo(t.data)<0){
        	t.left = insert(x,t.left);
        	if(height(t.left)-height(t.right)==2)
        		if(x.compareTo(t.left.data)<0)
        			t=rotateWithLeftChild(t);
        		else
        			t = doubleWithLeftChild(t);
        }
        else{
        	System.out.println("x is already exist, try to insert other object");
        }
    	 t.height = max(height(t.left), height(t.right))+1;
         return t;	
    }
    /* Rotate binary tree node with left child */
    private AVLNode rotateWithLeftChild(AVLNode k2)
    {
        AVLNode krotate = k2.left;
        k2.left= krotate.right;
        krotate.right= k2;
        k2.height = max(height(k2.right), height(k2.left)) +1;
        krotate.height = max(k2.height,(height(krotate.left))) +1;
        return krotate;
    }

    /* Rotate binary tree node with right child */
    private AVLNode rotateWithRightChild(AVLNode k1)
    {
        AVLNode krotate= k1.right;
        k1.right = krotate.left;
        krotate.left= k1;
        k1.height = max(height(k1.left),height(k1.right)) +1;
        krotate.height = max( height( krotate.right ), k1.height ) + 1;
        return krotate;
    }
    /**
     * Double rotate binary tree node: first left child
     * with its right child; then node k3 with new left child */
    private AVLNode doubleWithLeftChild(AVLNode k3)
    {
    	k3.left = rotateWithRightChild(k3.left);
        return rotateWithLeftChild(k3);
    }
    /**
     * Double rotate binary tree node: first right child
     * with its left child; then node k1 with new right child */
    private AVLNode doubleWithRightChild(AVLNode k1)
    {
        k1.right = rotateWithLeftChild(k1.right);
        return rotateWithRightChild(k1);
    }
    /* Functions to count number of nodes */
    public int countNodes()
    {
    	return countNodes(root) +1;
    }
    private int countNodes(AVLNode r)
    {
	    int counter =0;
	    if(r!= null)
	    	counter = countNodes(r.left) +countNodes(r.right) +1;
	    return counter;
    }
    /* Functions to search for an element */
    public boolean search(Comparable val)
    {
    	return search(root,val);
    }
    private boolean search(AVLNode r, Comparable val)
    {
    	if(r == null)
    		return false;
    	else{
	        if(val.compareTo(r.data)>0)
	     	   return search(r.right,val);
	        else if(val.compareTo(r.data)<0)
	     	   return search(r.left,val);
	        else
	     	   return true;
    	}
    }
    /* Function for inorder traversal */
    public void inorder(PrintWriter out)
    {
    	inorder(root,out);
    }
    private void inorder(AVLNode r, PrintWriter out)
    {
    	while(r!=null)
    		if(r.right!= null)
    			inorder(r.right,out);
    		out.print(r.data);
    		if(r.left!= null)
    			inorder(r.left,out);
    }

    public int[] getPrivateKey(String sIndex) {
        //Complete Your Code Here
    }

    private int[] getPrivateKey(AVLNode r, String sIndex) {
        //Complete Your Code Here
    }
}